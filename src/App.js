import React from "react";

import { Router, Switch, Route } from "react-router-dom";
import history from "./history";

import "./App.scss";
import Home from "./pages/Home";
import NewMember from "./pages/NewMember";
import User from "./pages/User";

function App() {
  return (
    <div className="App">
      <Router history={history}>
        <Switch>
          <Route path="/" component={Home} exact />
          <Route path="/create" component={NewMember} />
          <Route path="/user/:id" component={User} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
