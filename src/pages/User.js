import React from "react";
import Hero from "../components/Hero";
import Layout from "../components/Layout";
import MemberEditor from "../components/MemberEditor";

const User = (routerProps) => {
  const id = routerProps.match.params.id;
  return (
    <Layout>
      <Hero half />
      <MemberEditor id={id} />
    </Layout>
  );
};

export default User;
