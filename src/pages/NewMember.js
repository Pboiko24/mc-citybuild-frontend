import React from "react";
import Hero from "../components/Hero";
import Layout from "../components/Layout";
import MemberCreator from "../components/MemberCreator";

const NewMember = () => {
  return (
    <Layout>
      <Hero half />
      <MemberCreator />
    </Layout>
  );
};

export default NewMember;
