import React from "react";
import AboutUs from "../components/AboutUs";
import Hero from "../components/Hero";
import Layout from "../components/Layout";
import Members from "../components/Members";
import SocialMedia from "../components/SocialMedia";

const Home = () => {
  return (
    <div className="Home">
      <Layout>
        <Hero />
        <AboutUs />
        <SocialMedia />
        <Members />
      </Layout>
    </div>
  );
};

export default Home;
