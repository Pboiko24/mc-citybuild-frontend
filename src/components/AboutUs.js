import React from "react";

const AboutUs = () => {
  return (
    <div className="AboutUs">
      <div className="container">
        <div className="row">
          <div className="col">
            <section className="we p-5">
              <span className="title">Das Sind Wir!</span>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque
                laboriosam voluptatem laudantium eveniet nobis nihil ut nisi.
                Optio suscipit ipsam quae illo corrupti, ducimus sit ex
                repudiandae dolores, accusantium autem.
              </p>
            </section>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AboutUs;
