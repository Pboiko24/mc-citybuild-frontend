import React from "react";
import axios from "axios";

const Members = () => {
  const [members, setMembers] = React.useState([]);
  React.useEffect(() => {
    axios.get("http://localhost:5000/members/all").then((result) => {
      setMembers(result.data);
    });
  }, []);
  return (
    <div className="Members pt-5">
      <div className="container">
        <div className="title">Members</div>
        <div className="cards">
          <div className="row pt-5">
            {members.map((member, idx) => {
              return (
                <div className="col-4 text-center" key={idx}>
                  <div className="member">
                    <img className="w-75" src={member.img} alt="avatar" />
                    <br />
                    <div>
                      <span>
                        <a href={"http://localhost:3000/user/" + member.id}>
                          {" "}
                          {member.name} x {member.group_name}
                        </a>
                      </span>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Members;
