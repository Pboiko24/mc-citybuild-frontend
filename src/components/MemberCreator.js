import React from "react";
import axios from "axios";

const MemberCreator = () => {
  const [name, setName] = React.useState("");
  const [group_name, setGroup_name] = React.useState("");
  const [img, setImg] = React.useState("");
  const [message, setMessage] = React.useState("");

  const handleSubmit = (event) => {
    event.preventDefault();

    axios
      .post("http://localhost:5000/members/create", {
        name: name,
        group_name: group_name,
        img: img,
      })
      .then((message) => {
        setMessage(message.data);
      });
  };

  return (
    <div className="NewMember">
      <div className="container text-center">
        <div className="title">Neues Mitglied hinzufügen</div>
        <form onSubmit={handleSubmit}>
          <div className="p-5">{message}</div>
          <input
            type="text"
            placeholder="Name"
            onChange={(event) => setName(event.target.value)}
          />
          <div className="pt-3"></div>
          <input
            type="text"
            placeholder="group_name"
            onChange={(event) => setGroup_name(event.target.value)}
          />
          <div className="pt-3"></div>
          <input
            type="text"
            placeholder="img"
            onChange={(event) => setImg(event.target.value)}
          />
          <div className="pt-5"></div>
          <button type="submit" className="submit">
            erstellen
          </button>
        </form>
      </div>
    </div>
  );
};

export default MemberCreator;
