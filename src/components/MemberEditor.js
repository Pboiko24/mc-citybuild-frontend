import React from "react";
import axios from "axios";

const MemberEditor = ({ id }) => {
  const [name, setName] = React.useState("");
  const [group_name, setGroup_name] = React.useState("");
  const [img, setImg] = React.useState("");
  const [message, setMessage] = React.useState("");

  const handleSubmit = (event) => {
    event.preventDefault();

    axios
      .patch("http://localhost:5000/members", {
        id: id,
        name: name,
        group_name: group_name,
        img: img,
      })
      .then((message) => {
        setMessage(message.data);
      });
  };

  return (
    <div className="MemberEditor">
      <div className="container text-center">
        <form onSubmit={handleSubmit}>
          <div className="p-5">{message}</div>
          <input
            type="text"
            placeholder="Name"
            onChange={(event) => setName(event.target.value)}
          />
          <div className="pt-3"></div>
          <input
            type="text"
            placeholder="group_name"
            onChange={(event) => setGroup_name(event.target.value)}
          />
          <div className="pt-3"></div>
          <input
            type="text"
            placeholder="img"
            onChange={(event) => setImg(event.target.value)}
          />
          <div className="pt-5"></div>
          <button type="submit" className="submit">
            updaten
          </button>
        </form>
      </div>
    </div>
  );
};

export default MemberEditor;
