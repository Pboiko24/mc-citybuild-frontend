import React from "react";

const Hero = ({ half }) => {
  if (half) {
    return (
      <div className="Hero-half">
        <div className="Hero-overlay">
          <div className="Hero-title">
            <h1>MC-Citybuild</h1>
            <span>CityBuild anderst.</span>
          </div>
        </div>
      </div>
    );
  }

  return (
    <div className="Hero">
      <div className="Hero-overlay">
        <div className="Hero-title">
          <h1>MC-Citybuild</h1>
          <span>CityBuild anderst.</span>
        </div>
      </div>
    </div>
  );
};

export default Hero;
