import React from "react";
import axios from "axios";

const Navbar = () => {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark fixed-top pt-4">
      <div className="container">
        <a
          className="navbar-brand ml-auto mr-auto"
          href="http://localhost:3000"
        >
          <strong>MC-Citybuild</strong>
        </a>
        <a
          onClick={() => {
            axios.post("http://localhost:5000/setup");
            console.log("TEst");
          }}
        >
          setup backend
        </a>
      </div>
    </nav>
  );
};

export default Navbar;
