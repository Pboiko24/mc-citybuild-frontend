import React from "react";

const SocialMedia = () => {
  return (
    <div className="SocialMedia">
      <div className="container text-center p-5">
        <div className="row">
          <div className="col p-5">
            <section className="why">
              <span className="title">Das macht uns einzigartig!</span>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque
                laboriosam voluptatem laudantium eveniet nobis nihil ut nisi.
                Optio suscipit ipsam quae illo corrupti, ducimus sit ex
                repudiandae dolores, accusantium autem.
              </p>
            </section>
          </div>
          <div className="col">
            <div>
              <button className="forum">Forum</button>
            </div>
            <div>
              <button className="teamspeak">Teamspeak</button>
            </div>
            <div>
              <button className="discord">Discord</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SocialMedia;
